package com.example.mynativecplus;

public class CProduct {

    public CProduct() {
        incode = "";
        fname = "";
        price = 0;
        qty = 0;
    }

    //商品编码
    public String incode;
    //商品名称
    public String fname;
    //价格
    public float price;
    //数量
    public int qty;

    @Override
    public String toString() {
        return "CProduct{" +
                "incode='" + incode + '\'' +
                ", fname='" + fname + '\'' +
                ", price=" + price +
                ", qty=" + qty +
                '}';
    }
}