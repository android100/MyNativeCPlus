package com.example.mynativecplus;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = MainActivity.class.getSimpleName();

    // Used to load the 'native-lib' library on application startup.
    static {
        System.loadLibrary("native-lib");
    }

    private TextView mTv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Example of a call to a native method
        mTv = (TextView) findViewById(R.id.sample_text);
        mTv.setText("" + dynamicFromJNI());
    }

    public void showText() {
        mTv.setText("我是Java方法");
    }

    public void showText2(String str) {
        mTv.setText(str);
    }

    /**
     * A native method that is implemented by the 'native-lib' native library,
     * which is packaged with this application.
     */
    public native String stringFromJNI();

    public native void sayHello(); //动态注册

    public native String dynamicFromJNI();  //动态注册

    public native float dynamicFromJNI2(float x, float y);  //动态注册 并参数

    public native String settextgood(String str);

    public native void showtextFromJNI();

    public native void showtextFromJNI2();

    //获取产品信息
    public native static CProduct getproduct();

    //更新产品信息
    public native static CProduct updateproduct(CProduct prd);

    public native static List<CProduct> getlistproduct();

    public native static List<CProduct> updatelistproduct(List<CProduct> list);

    public static native void callJavaStaticMethod();

    public static native void callJavaInstaceMethod();

    @SuppressLint("NonConstantResourceId")
    public void btnclick(View view) {
        switch (view.getId()) {
            case R.id.btn1:
                Log.e(TAG, "onCreate: getproduct = " + getproduct().toString());
                mTv.setText(getproduct().toString());
                break;

            case R.id.btn2:
                CProduct cProduct = new CProduct();
                cProduct.fname = "计时水壶";
                cProduct.incode = "001";
                cProduct.price = 90;
                cProduct.qty = 10;
                Log.e(TAG, "onCreate: updateproduct = " + updateproduct(cProduct).toString());
                mTv.setText(updateproduct(cProduct).toString());
                break;

            case R.id.btn3:
                callJavaInstaceMethod();
                break;

            case R.id.btn4:
                callJavaStaticMethod();
                break;

            case R.id.btn5:
                mTv.setText(settextgood("xq"));
                break;

            case R.id.btn6:
                showtextFromJNI();
                break;

            case R.id.btn7:
                showtextFromJNI2();
                break;

            case R.id.btn8:
                List<CProduct> products = getlistproduct();
                for (int i = 0; i < products.size(); i++) {
                    Log.e(TAG, "product =  " + products.get(i).toString());
                }
                break;

            case R.id.btn9:
                List<CProduct> productList = new ArrayList<>();
                for (int i = 0; i < 5; i++) {
                    productList.add(new CProduct());
                }
                Log.e(TAG, "btnclick:productList =  " + productList.size());
                List<CProduct> updatelistproduct = updatelistproduct(productList);
                Log.e(TAG, "btnclick:updatelistproduct =  " + updatelistproduct.size());
                for (int i = 0; i < updatelistproduct.size(); i++) {
                    Log.e(TAG, i + " = item =  " + updatelistproduct.get(i).toString());
                }
                break;

            default:
                break;
        }
    }
}