


# 创建cmake项目：

这里选择native c++

![](imgs/01.png)

这里默认即可

![](imgs/02.png)

这里选择`c++11`

![](imgs/03.png)

# native方法参数详解

```c

extern "C"
JNIEXPORT void JNICALL
Java_com_example_mynativecplus_MainActivity_callJavaStaticMethod(JNIEnv *env, jclass clazz) {
    //...
}

```
extern "C"：

Java_com_example_mynativecplus_MainActivity_callJavaStaticMethod : 表示native方法定义在哪里，叫什么名字

JNIEnv *env：

jclass clazz ： 表示native方法所在类

# javap 的 使用 : 查看描述符

```shell
> javap -help
用法: javap <options> <classes>
其中, 可能的选项包括:
  -help  --help  -?        输出此用法消息
  -version                 版本信息
  -v  -verbose             输出附加信息
  -l                       输出行号和本地变量表
  -public                  仅显示公共类和成员
  -protected               显示受保护的/公共类和成员
  -package                 显示程序包/受保护的/公共类
                           和成员 (默认)
  -p  -private             显示所有类和成员
  -c                       对代码进行反汇编
  -s                       输出内部类型签名
  -sysinfo                 显示正在处理的类的
                           系统信息 (路径, 大小, 日期, MD5 散列)
  -constants               显示最终常量
  -classpath <path>        指定查找用户类文件的位置
  -cp <path>               指定查找用户类文件的位置
  -bootclasspath <path>    覆盖引导类文件的位置
```

`build/intermediates/javac`/debug/classes/com/example/mynativecplus找到class文件，然后当前文件夹下打开命令行，执行命令：

```shell
> javap -s CProduct
警告: 二进制文件CProduct包含com.example.mynativecplus.CProduct
Compiled from "CProduct.java"
public class com.example.mynativecplus.CProduct {
public java.lang.String incode;
  descriptor: Ljava/lang/String;
public java.lang.String fname;
  descriptor: Ljava/lang/String;
public float price;
  descriptor: F
public int qty;
  descriptor: I
public com.example.mynativecplus.CProduct();
  descriptor: ()V

public java.lang.String toString();
  descriptor: ()Ljava/lang/String;
}
```

# native中使用Android log方法

```c

#include "android/log.h"

#ifndef  LOG_TAG
#define  LOG_TAG    "Log4AndroidJNI"
#define  LOGI(...)  __android_log_print(ANDROID_LOG_INFO,LOG_TAG,__VA_ARGS__)
#define  LOGD(...)  __android_log_print(ANDROID_LOG_DEBUG,LOG_TAG,__VA_ARGS__)
#define  LOGW(...)  __android_log_print(ANDROID_LOG_WARN,LOG_TAG,__VA_ARGS__)
#define  LOGE(...)  __android_log_print(ANDROID_LOG_ERROR,LOG_TAG,__VA_ARGS__)
#define  LOGV(...)  __android_log_print(ANDROID_LOG_VERBOSE,LOG_TAG,__VA_ARGS__)
#endif


printf("%s", "size === " + size);
//输出传入的a和b参数
LOGI("size=%d",size);
LOGE("size=%d",size);
LOGW("size=%d",size);
LOGD("size=%d",size);
LOGV("size=%d",size);
```

log 添加文件名，方法名，行号 ： 
```c
#include "android/log.h"

// Windows 和 Linux 这两个宏是在 CMakeLists.txt 通过 ADD_DEFINITIONS 定义的
#ifdef Windows
#define __FILENAME__ (strrchr(__FILE__, '\\') + 1) // Windows下文件目录层级是'\\'
#elif Linux
#define __FILENAME__ (strrchr(__FILE__, '/') + 1) // Linux下文件目录层级是'/'
#else
#define __FILENAME__ (strrchr(__FILE__, '/') + 1) // 默认使用这种方式
#endif

#ifndef  LOG_TAG
#define  LOG_TAG    "Log4AndroidJNI"
#define  LOGI(format, ...) __android_log_print(ANDROID_LOG_INFO, LOG_TAG,\
        "[%s][%s][%d]: " format, __FILENAME__, __FUNCTION__, __LINE__, ##__VA_ARGS__);
#define  LOGD(format, ...) __android_log_print(ANDROID_LOG_DEBUG, LOG_TAG,\
        "[%s][%s][%d]: " format, __FILENAME__, __FUNCTION__, __LINE__, ##__VA_ARGS__);
#define  LOGW(format, ...) __android_log_print(ANDROID_LOG_WARN, LOG_TAG,\
        "[%s][%s][%d]: " format, __FILENAME__, __FUNCTION__, __LINE__, ##__VA_ARGS__);
#define  LOGE(format, ...) __android_log_print(ANDROID_LOG_ERROR, LOG_TAG,\
        "[%s][%s][%d]: " format, __FILENAME__, __FUNCTION__, __LINE__, ##__VA_ARGS__);
#define  LOGV(format, ...) __android_log_print(ANDROID_LOG_VERBOSE, LOG_TAG,\
        "[%s][%s][%d]: " format, __FILENAME__, __FUNCTION__, __LINE__, ##__VA_ARGS__);
#endif


//使用：
LOGI("size=%d", size);
    LOGE("size=%d", size);
    LOGW("size=%d", size);
    LOGD("size=%d", size);
    LOGV("size=%d", size);
```


# addr2line 追踪so库错误

Android studio中添加日志过滤：

![](imgs/1.png)

![](imgs/2.png)


```c
//    int* p = 0; //空指针
//    *p = 1; //写空指针指向的内存，产生SIGSEGV信号，造成Crash
```

过滤后看崩溃信息：

```shell script
2021-05-27 11:54:30.528 5244-5244/? A/DEBUG: *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** ***
2021-05-27 11:54:30.528 5244-5244/? A/DEBUG: Build fingerprint: 'samsung/astarqltezc/astarqltechn:10/QP1A.190711.020/G8850ZCU5CTL1:user/release-keys'
2021-05-27 11:54:30.528 5244-5244/? A/DEBUG: Revision: '0'
2021-05-27 11:54:30.528 5244-5244/? A/DEBUG: ABI: 'arm64'
2021-05-27 11:54:30.530 5244-5244/? A/DEBUG: Timestamp: 2021-05-27 11:54:30+0800
2021-05-27 11:54:30.531 5244-5244/? A/DEBUG: pid: 5189, tid: 5189, name: e.mynativecplus  >>> com.example.mynativecplus <<<
2021-05-27 11:54:30.531 5244-5244/? A/DEBUG: uid: 10842
2021-05-27 11:54:30.531 5244-5244/? A/DEBUG: signal 11 (SIGSEGV), code 1 (SEGV_MAPERR), fault addr 0x0
2021-05-27 11:54:30.531 5244-5244/? A/DEBUG: Cause: null pointer dereference
2021-05-27 11:54:30.531 5244-5244/? A/DEBUG:     x0  0000007ff946123f  x1  0000007ff9461184  x2  000000000000000e  x3  63002b2b43206d6f
2021-05-27 11:54:30.531 5244-5244/? A/DEBUG:     x4  0000007a87b70336  x5  0000007ff946123f  x6  7266206f6c6c6548  x7  2b2b43206d6f7266
2021-05-27 11:54:30.531 5244-5244/? A/DEBUG:     x8  0000000000000000  x9  0000000000000001  x10 0000000000000000  x11 000000000000000e
2021-05-27 11:54:30.531 5244-5244/? A/DEBUG:     x12 0000000000000001  x13 000000000048a440  x14 0000000000000006  x15 ffffffffffffffff
2021-05-27 11:54:30.531 5244-5244/? A/DEBUG:     x16 0000007a87b7cc58  x17 0000007a87b57730  x18 0000007b21f20000  x19 0000007b20e72000
2021-05-27 11:54:30.531 5244-5244/? A/DEBUG:     x20 0000000000000000  x21 0000007b20e72000  x22 0000007ff94614f0  x23 0000007a8b3c9b61
2021-05-27 11:54:30.531 5244-5244/? A/DEBUG:     x24 0000000000000004  x25 0000007b21006020  x26 0000007b20e720b0  x27 0000000000000001
2021-05-27 11:54:30.531 5244-5244/? A/DEBUG:     x28 0000007ff9461280  x29 0000007ff9461250
2021-05-27 11:54:30.531 5244-5244/? A/DEBUG:     sp  0000007ff94611e0  lr  0000007a87b55360  pc  0000007a87b55370
2021-05-27 11:54:30.917 5244-5244/? A/DEBUG: backtrace:
2021-05-27 11:54:30.917 5244-5244/? A/DEBUG:       #00 pc 000000000000f370  /data/app/com.example.mynativecplus-y5z2kpdoHpxc4LAmzkMPUg==/base.apk!libnative-lib.so (offset 0x56000) (Java_com_example_mynativecplus_MainActivity_stringFromJNI+72) (BuildId: d20eee540d6384d4c3aa4220b1a55048d819029a)
2021-05-27 11:54:30.918 5244-5244/? A/DEBUG:       #01 pc 0000000000140350  /apex/com.android.runtime/lib64/libart.so (art_quick_generic_jni_trampoline+144) (BuildId: a372d96691d23073eecb40fdc2260253)
2021-05-27 11:54:30.918 5244-5244/? A/DEBUG:       #02 pc 0000000000137334  /apex/com.android.runtime/lib64/libart.so (art_quick_invoke_stub+548) (BuildId: a372d96691d23073eecb40fdc2260253)
2021-05-27 11:54:30.918 5244-5244/? A/DEBUG:       #03 pc 0000000000145fec  /apex/com.android.runtime/lib64/libart.so (art::ArtMethod::Invoke(art::Thread*, unsigned int*, unsigned int, art::JValue*, char const*)+244) (BuildId: a372d96691d23073eecb40fdc2260253)
2021-05-27 11:54:30.918 5244-5244/? A/DEBUG:       #04 pc 00000000002e3d74  /apex/com.android.runtime/lib64/libart.so (art::interpreter::ArtInterpreterToCompiledCodeBridge(art::Thread*, art::ArtMethod*, art::ShadowFrame*, unsigned short, art::JValue*)+384) (BuildId: a372d96691d23073eecb40fdc2260253)
2021-05-27 11:54:30.918 5244-5244/? A/DEBUG:       #05 pc 00000000002defd4  /apex/com.android.runtime/lib64/libart.so (bool art::interpreter::DoCall<false, false>(art::ArtMethod*, art::Thread*, art::ShadowFrame&, art::Instruction const*, unsigned short, art::JValue*)+892) (BuildId: a372d96691d23073eecb40fdc2260253)
2021-05-27 11:54:30.918 5244-5244/? A/DEBUG:       #06 pc 00000000005a1bcc  /apex/com.android.runtime/lib64/libart.so (MterpInvokeVirtual+648) (BuildId: a372d96691d23073eecb40fdc2260253)
2021-05-27 11:54:30.918 5244-5244/? A/DEBUG:       #07 pc 0000000000131814  /apex/com.android.runtime/lib64/libart.so (mterp_op_invoke_virtual+20) (BuildId: a372d96691d23073eecb40fdc2260253)
2021-05-27 11:54:30.918 5244-5244/? A/DEBUG:       #08 pc 000000000002c7c0  [anon:dalvik-classes2.dex extracted in memory from /data/app/com.example.mynativecplus-y5z2kpdoHpxc4LAmzkMPUg==/base.apk!classes2.dex] (com.example.mynativecplus.MainActivity.onCreate+40)
2021-05-27 11:54:30.918 5244-5244/? A/DEBUG:       #09 pc 00000000005a1e8c  /apex/com.android.runtime/lib64/libart.so (MterpInvokeVirtual+1352) (BuildId: a372d96691d23073eecb40fdc2260253)
2021-05-27 11:54:30.918 5244-5244/? A/DEBUG:       #10 pc 0000000000131814  /apex/com.android.runtime/lib64/libart.so (mterp_op_invoke_virtual+20) (BuildId: a372d96691d23073eecb40fdc2260253)
2021-05-27 11:54:30.918 5244-5244/? A/DEBUG:       #11 pc 00000000001aff1a  /system/framework/framework.jar (android.app.Activity.performCreate+30)
2021-05-27 11:54:30.918 5244-5244/? A/DEBUG:       #12 pc 00000000005a1e8c  /apex/com.android.runtime/lib64/libart.so (MterpInvokeVirtual+1352) (BuildId: a372d96691d23073eecb40fdc2260253)
2021-05-27 11:54:30.918 5244-5244/? A/DEBUG:       #13 pc 0000000000131814  /apex/com.android.runtime/lib64/libart.so (mterp_op_invoke_virtual+20) (BuildId: a372d96691d23073eecb40fdc2260253)
2021-05-27 11:54:30.918 5244-5244/? A/DEBUG:       #14 pc 00000000001afee2  /system/framework/framework.jar (android.app.Activity.performCreate+2)
2021-05-27 11:54:30.918 5244-5244/? A/DEBUG:       #15 pc 00000000005a1e8c  /apex/com.android.runtime/lib64/libart.so (MterpInvokeVirtual+1352) (BuildId: a372d96691d23073eecb40fdc2260253)
2021-05-27 11:54:30.918 5244-5244/? A/DEBUG:       #16 pc 0000000000131814  /apex/com.android.runtime/lib64/libart.so (mterp_op_invoke_virtual+20) (BuildId: a372d96691d23073eecb40fdc2260253)
2021-05-27 11:54:30.918 5244-5244/? A/DEBUG:       #17 pc 000000000021e1a2  /system/framework/framework.jar (android.app.Instrumentation.callActivityOnCreate+6)
2021-05-27 11:54:30.919 5244-5244/? A/DEBUG:       #18 pc 00000000005a1e8c  /apex/com.android.runtime/lib64/libart.so (MterpInvokeVirtual+1352) (BuildId: a372d96691d23073eecb40fdc2260253)
2021-05-27 11:54:30.919 5244-5244/? A/DEBUG:       #19 pc 0000000000131814  /apex/com.android.runtime/lib64/libart.so (mterp_op_invoke_virtual+20) (BuildId: a372d96691d23073eecb40fdc2260253)
2021-05-27 11:54:30.919 5244-5244/? A/DEBUG:       #20 pc 000000000019e2ee  /system/framework/framework.jar (android.app.ActivityThread.performLaunchActivity+902)
2021-05-27 11:54:30.919 5244-5244/? A/DEBUG:       #21 pc 00000000005a4198  /apex/com.android.runtime/lib64/libart.so (MterpInvokeDirect+1100) (BuildId: a372d96691d23073eecb40fdc2260253)
2021-05-27 11:54:30.919 5244-5244/? A/DEBUG:       #22 pc 0000000000131914  /apex/com.android.runtime/lib64/libart.so (mterp_op_invoke_direct+20) (BuildId: a372d96691d23073eecb40fdc2260253)
2021-05-27 11:54:30.919 5244-5244/? A/DEBUG:       #23 pc 000000000019ded6  /system/framework/framework.jar (android.app.ActivityThread.handleLaunchActivity+94)
2021-05-27 11:54:30.919 5244-5244/? A/DEBUG:       #24 pc 00000000005a1e8c  /apex/com.android.runtime/lib64/libart.so (MterpInvokeVirtual+1352) (BuildId: a372d96691d23073eecb40fdc2260253)
2021-05-27 11:54:30.919 5244-5244/? A/DEBUG:       #25 pc 0000000000131814  /apex/com.android.runtime/lib64/libart.so (mterp_op_invoke_virtual+20) (BuildId: a372d96691d23073eecb40fdc2260253)
2021-05-27 11:54:30.919 5244-5244/? A/DEBUG:       #26 pc 00000000002a7b26  /system/framework/framework.jar (android.app.servertransaction.LaunchActivityItem.execute+126)
2021-05-27 11:54:30.919 5244-5244/? A/DEBUG:       #27 pc 00000000005a1e8c  /apex/com.android.runtime/lib64/libart.so (MterpInvokeVirtual+1352) (BuildId: a372d96691d23073eecb40fdc2260253)
2021-05-27 11:54:30.919 5244-5244/? A/DEBUG:       #28 pc 0000000000131814  /apex/com.android.runtime/lib64/libart.so (mterp_op_invoke_virtual+20) (BuildId: a372d96691d23073eecb40fdc2260253)
2021-05-27 11:54:30.919 5244-5244/? A/DEBUG:       #29 pc 00000000002aa072  /system/framework/framework.jar (android.app.servertransaction.TransactionExecutor.executeCallbacks+154)
2021-05-27 11:54:30.919 5244-5244/? A/DEBUG:       #30 pc 00000000005a1e8c  /apex/com.android.runtime/lib64/libart.so (MterpInvokeVirtual+1352) (BuildId: a372d96691d23073eecb40fdc2260253)
2021-05-27 11:54:30.919 5244-5244/? A/DEBUG:       #31 pc 0000000000131814  /apex/com.android.runtime/lib64/libart.so (mterp_op_invoke_virtual+20) (BuildId: a372d96691d23073eecb40fdc2260253)
2021-05-27 11:54:30.919 5244-5244/? A/DEBUG:       #32 pc 00000000002a9fae  /system/framework/framework.jar (android.app.servertransaction.TransactionExecutor.execute+146)
2021-05-27 11:54:30.919 5244-5244/? A/DEBUG:       #33 pc 00000000005a1e8c  /apex/com.android.runtime/lib64/libart.so (MterpInvokeVirtual+1352) (BuildId: a372d96691d23073eecb40fdc2260253)
2021-05-27 11:54:30.919 5244-5244/? A/DEBUG:       #34 pc 0000000000131814  /apex/com.android.runtime/lib64/libart.so (mterp_op_invoke_virtual+20) (BuildId: a372d96691d23073eecb40fdc2260253)
2021-05-27 11:54:30.919 5244-5244/? A/DEBUG:       #35 pc 000000000019d180  /system/framework/framework.jar (android.app.ActivityThread$H.handleMessage+152)
2021-05-27 11:54:30.919 5244-5244/? A/DEBUG:       #36 pc 00000000005a1e8c  /apex/com.android.runtime/lib64/libart.so (MterpInvokeVirtual+1352) (BuildId: a372d96691d23073eecb40fdc2260253)
2021-05-27 11:54:30.919 5244-5244/? A/DEBUG:       #37 pc 0000000000131814  /apex/com.android.runtime/lib64/libart.so (mterp_op_invoke_virtual+20) (BuildId: a372d96691d23073eecb40fdc2260253)
2021-05-27 11:54:30.920 5244-5244/? A/DEBUG:       #38 pc 000000000035bc32  /system/framework/framework.jar (android.os.Handler.dispatchMessage+38)
2021-05-27 11:54:30.920 5244-5244/? A/DEBUG:       #39 pc 00000000005a1e8c  /apex/com.android.runtime/lib64/libart.so (MterpInvokeVirtual+1352) (BuildId: a372d96691d23073eecb40fdc2260253)
2021-05-27 11:54:30.920 5244-5244/? A/DEBUG:       #40 pc 0000000000131814  /apex/com.android.runtime/lib64/libart.so (mterp_op_invoke_virtual+20) (BuildId: a372d96691d23073eecb40fdc2260253)
2021-05-27 11:54:30.920 5244-5244/? A/DEBUG:       #41 pc 000000000039bca6  /system/framework/framework.jar (android.os.Looper.loop+466)
2021-05-27 11:54:30.920 5244-5244/? A/DEBUG:       #42 pc 00000000005a493c  /apex/com.android.runtime/lib64/libart.so (MterpInvokeStatic+1040) (BuildId: a372d96691d23073eecb40fdc2260253)
2021-05-27 11:54:30.920 5244-5244/? A/DEBUG:       #43 pc 0000000000131994  /apex/com.android.runtime/lib64/libart.so (mterp_op_invoke_static+20) (BuildId: a372d96691d23073eecb40fdc2260253)
2021-05-27 11:54:30.920 5244-5244/? A/DEBUG:       #44 pc 00000000001a75e4  /system/framework/framework.jar (android.app.ActivityThread.main+208)
2021-05-27 11:54:30.920 5244-5244/? A/DEBUG:       #45 pc 00000000002b5088  /apex/com.android.runtime/lib64/libart.so (_ZN3art11interpreterL7ExecuteEPNS_6ThreadERKNS_20CodeItemDataAccessorERNS_11ShadowFrameENS_6JValueEbb.llvm.12652868791225418157+240) (BuildId: a372d96691d23073eecb40fdc2260253)
2021-05-27 11:54:30.920 5244-5244/? A/DEBUG:       #46 pc 0000000000593134  /apex/com.android.runtime/lib64/libart.so (artQuickToInterpreterBridge+1032) (BuildId: a372d96691d23073eecb40fdc2260253)
2021-05-27 11:54:30.920 5244-5244/? A/DEBUG:       #47 pc 0000000000140468  /apex/com.android.runtime/lib64/libart.so (art_quick_to_interpreter_bridge+88) (BuildId: a372d96691d23073eecb40fdc2260253)
2021-05-27 11:54:30.920 5244-5244/? A/DEBUG:       #48 pc 00000000001375b8  /apex/com.android.runtime/lib64/libart.so (art_quick_invoke_static_stub+568) (BuildId: a372d96691d23073eecb40fdc2260253)
2021-05-27 11:54:30.920 5244-5244/? A/DEBUG:       #49 pc 000000000014600c  /apex/com.android.runtime/lib64/libart.so (art::ArtMethod::Invoke(art::Thread*, unsigned int*, unsigned int, art::JValue*, char const*)+276) (BuildId: a372d96691d23073eecb40fdc2260253)
2021-05-27 11:54:30.920 5244-5244/? A/DEBUG:       #50 pc 00000000004b1864  /apex/com.android.runtime/lib64/libart.so (art::(anonymous namespace)::InvokeWithArgArray(art::ScopedObjectAccessAlreadyRunnable const&, art::ArtMethod*, art::(anonymous namespace)::ArgArray*, art::JValue*, char const*)+104) (BuildId: a372d96691d23073eecb40fdc2260253)
2021-05-27 11:54:30.920 5244-5244/? A/DEBUG:       #51 pc 00000000004b3408  /apex/com.android.runtime/lib64/libart.so (art::InvokeMethod(art::ScopedObjectAccessAlreadyRunnable const&, _jobject*, _jobject*, _jobject*, unsigned long)+1480) (BuildId: a372d96691d23073eecb40fdc2260253)
2021-05-27 11:54:30.920 5244-5244/? A/DEBUG:       #52 pc 000000000043e1e8  /apex/com.android.runtime/lib64/libart.so (art::Method_invoke(_JNIEnv*, _jobject*, _jobject*, _jobjectArray*)+52) (BuildId: a372d96691d23073eecb40fdc2260253)
2021-05-27 11:54:30.920 5244-5244/? A/DEBUG:       #53 pc 00000000000c2d34  /system/framework/arm64/boot.oat (art_jni_trampoline+180) (BuildId: 762429e6bbcad454679183b0c0bd3d69afa06c1e)
2021-05-27 11:54:30.920 5244-5244/? A/DEBUG:       #54 pc 0000000000137334  /apex/com.android.runtime/lib64/libart.so (art_quick_invoke_stub+548) (BuildId: a372d96691d23073eecb40fdc2260253)
2021-05-27 11:54:30.920 5244-5244/? A/DEBUG:       #55 pc 0000000000145fec  /apex/com.android.runtime/lib64/libart.so (art::ArtMethod::Invoke(art::Thread*, unsigned int*, unsigned int, art::JValue*, char const*)+244) (BuildId: a372d96691d23073eecb40fdc2260253)
2021-05-27 11:54:30.920 5244-5244/? A/DEBUG:       #56 pc 00000000002e3d74  /apex/com.android.runtime/lib64/libart.so (art::interpreter::ArtInterpreterToCompiledCodeBridge(art::Thread*, art::ArtMethod*, art::ShadowFrame*, unsigned short, art::JValue*)+384) (BuildId: a372d96691d23073eecb40fdc2260253)
2021-05-27 11:54:30.920 5244-5244/? A/DEBUG:       #57 pc 00000000002defd4  /apex/com.android.runtime/lib64/libart.so (bool art::interpreter::DoCall<false, false>(art::ArtMethod*, art::Thread*, art::ShadowFrame&, art::Instruction const*, unsigned short, art::JValue*)+892) (BuildId: a372d96691d23073eecb40fdc2260253)
2021-05-27 11:54:30.920 5244-5244/? A/DEBUG:       #58 pc 00000000005a1bcc  /apex/com.android.runtime/lib64/libart.so (MterpInvokeVirtual+648) (BuildId: a372d96691d23073eecb40fdc2260253)
2021-05-27 11:54:30.921 5244-5244/? A/DEBUG:       #59 pc 0000000000131814  /apex/com.android.runtime/lib64/libart.so (mterp_op_invoke_virtual+20) (BuildId: a372d96691d23073eecb40fdc2260253)
2021-05-27 11:54:30.921 5244-5244/? A/DEBUG:       #60 pc 000000000048dcb6  /system/framework/framework.jar (com.android.internal.os.RuntimeInit$MethodAndArgsCaller.run+22)
2021-05-27 11:54:30.921 5244-5244/? A/DEBUG:       #61 pc 00000000002b5088  /apex/com.android.runtime/lib64/libart.so (_ZN3art11interpreterL7ExecuteEPNS_6ThreadERKNS_20CodeItemDataAccessorERNS_11ShadowFrameENS_6JValueEbb.llvm.12652868791225418157+240) (BuildId: a372d96691d23073eecb40fdc2260253)
2021-05-27 11:54:30.921 5244-5244/? A/DEBUG:       #62 pc 0000000000593134  /apex/com.android.runtime/lib64/libart.so (artQuickToInterpreterBridge+1032) (BuildId: a372d96691d23073eecb40fdc2260253)
2021-05-27 11:54:30.921 5244-5244/? A/DEBUG:       #63 pc 0000000000140468  /apex/com.android.runtime/lib64/libart.so (art_quick_to_interpreter_bridge+88) (BuildId: a372d96691d23073eecb40fdc2260253)
2021-05-27 11:54:30.921 5244-5244/? A/DEBUG:       #64 pc 00000000009c5af4  /system/framework/arm64/boot-framework.oat (com.android.internal.os.ZygoteInit.main+3124) (BuildId: 1cbe7be31c1b562cf1d71f1b351cddc7001a637d)
2021-05-27 11:54:30.921 5244-5244/? A/DEBUG:       #65 pc 00000000001375b8  /apex/com.android.runtime/lib64/libart.so (art_quick_invoke_static_stub+568) (BuildId: a372d96691d23073eecb40fdc2260253)
2021-05-27 11:54:30.921 5244-5244/? A/DEBUG:       #66 pc 000000000014600c  /apex/com.android.runtime/lib64/libart.so (art::ArtMethod::Invoke(art::Thread*, unsigned int*, unsigned int, art::JValue*, char const*)+276) (BuildId: a372d96691d23073eecb40fdc2260253)
2021-05-27 11:54:30.921 5244-5244/? A/DEBUG:       #67 pc 00000000004b1864  /apex/com.android.runtime/lib64/libart.so (art::(anonymous namespace)::InvokeWithArgArray(art::ScopedObjectAccessAlreadyRunnable const&, art::ArtMethod*, art::(anonymous namespace)::ArgArray*, art::JValue*, char const*)+104) (BuildId: a372d96691d23073eecb40fdc2260253)
2021-05-27 11:54:30.921 5244-5244/? A/DEBUG:       #68 pc 00000000004b1454  /apex/com.android.runtime/lib64/libart.so (art::InvokeWithVarArgs(art::ScopedObjectAccessAlreadyRunnable const&, _jobject*, _jmethodID*, std::__va_list)+408) (BuildId: a372d96691d23073eecb40fdc2260253)
2021-05-27 11:54:30.921 5244-5244/? A/DEBUG:       #69 pc 00000000003bbba4  /apex/com.android.runtime/lib64/libart.so (art::JNI::CallStaticVoidMethodV(_JNIEnv*, _jclass*, _jmethodID*, std::__va_list)+624) (BuildId: a372d96691d23073eecb40fdc2260253)
2021-05-27 11:54:30.922 5244-5244/? A/DEBUG:       #70 pc 00000000000ed9e4  /system/lib64/libandroid_runtime.so (_JNIEnv::CallStaticVoidMethod(_jclass*, _jmethodID*, ...)+116) (BuildId: e2bf8ffce8075a56da750181a2adfb26)
2021-05-27 11:54:30.922 5244-5244/? A/DEBUG:       #71 pc 00000000000f0a44  /system/lib64/libandroid_runtime.so (android::AndroidRuntime::start(char const*, android::Vector<android::String8> const&, bool)+792) (BuildId: e2bf8ffce8075a56da750181a2adfb26)
2021-05-27 11:54:30.923 5244-5244/? A/DEBUG:       #72 pc 00000000000034f8  /system/bin/app_process64 (main+1192) (BuildId: 290f1eff66c01586c58ff56b88630af9)
2021-05-27 11:54:30.923 5244-5244/? A/DEBUG:       #73 pc 000000000007e898  /apex/com.android.runtime/lib64/bionic/libc.so (__libc_init+108) (BuildId: 71404a7ed387b7bf9be53e3be1aeb5bd)
2021-05-27 11:54:45.394 5974-5340/? W/android.os.Debug: failed to get memory consumption info: -1
2021-05-27 11:59:09.582 14163-5970/? W/android.os.Debug: failed to get memory consumption info: -1
2021-05-27 11:59:22.356 12106-6009/? W/android.os.Debug: failed to get memory consumption info: -1
2021-05-27 11:59:45.476 5974-6068/? W/android.os.Debug: failed to get memory consumption info: -1
2021-05-27 12:04:09.685 14163-6786/? W/android.os.Debug: failed to get memory consumption info: -1
2021-05-27 12:04:22.451 12106-6815/? W/android.os.Debug: failed to get memory consumption info: -1
2021-05-27 12:04:45.574 5974-6880/? W/android.os.Debug: failed to get memory consumption info: -1
2021-05-27 12:09:09.744 14163-7619/? W/android.os.Debug: failed to get memory consumption info: -1
2021-05-27 12:09:22.552 12106-7651/? W/android.os.Debug: failed to get memory consumption info: -1
2021-05-27 12:09:45.591 5974-7707/? W/android.os.Debug: failed to get memory consumption info: -1
2021-05-27 12:14:09.763 14163-8328/? W/android.os.Debug: failed to get memory consumption info: -1
2021-05-27 12:14:22.607 12106-8354/? W/android.os.Debug: failed to get memory consumption info: -1
2021-05-27 12:14:45.650 5974-8406/? W/android.os.Debug: failed to get memory consumption info: -1

```

我们使用addr2line 查看崩溃位置：

```shell script
# [addr2line路径] -e [so库路径] [出错code1] [出错code2] 
/Users/tmk/Library/Android/sdk/ndk-bundle/toolchains/aarch64-linux-android-4.9/prebuilt/darwin-x86_64/bin/aarch64-linux-android-addr2line -e /Users/tmk/Documents/project/tmkapp/local_modules/react-native-speech/android/src/main/jniLibs/arm64-v8a/libspeech.so 000000000004566c 
# 执行完可看到错误行数
/Users/tmk/Downloads/MyNativeCPlus/app/src/main/cpp/native-lib.cpp:34
```

关于addr2line的几点补充：

- 如果可执行文件中没有包括调试符号，您将获得??:0 作为响应。 

- 使用addr2line需要附带符号表的so文件。

- addr2line工具并不是一定能将异常位置定位到行，还需要其它调试手段辅助。

- 如果c/c++库程序是使用Android studio开发，因为生成的so自带符号表，所以使用addr2line工具可以直接定位。



